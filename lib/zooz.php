<?php

require_once __DIR__ . "/zooz_exceptions.php";

class Zooz
{
	const USER_AGENT = "ZoozPHP/0.1";
	const OPTIONAL_OPENTRX = "userIdNumber,addressZip,firstName,lastName,countryCode,phoneNumber,email,userExtra,invoiceNumber,invoiceDetails,invoiceAdditionalDetails";

	public $isSandbox = true;
	public $headers = array();

	public $zoozAppID = "";
	public $zoozAppKey = "";
	public $zoozURL = "";

	protected $curl = null;

	public function __construct($appid, $appkey, $sandbox = false)
	{
		$this->curl = curl_init();
		
		$this->isSandbox = $sandbox;
		if ($this->isSandbox == true) 
		{
			$this->zoozURL = 'https://sandbox.zooz.co/mobile/SecuredWebServlet';
		}
		else
		{
			$this->zoozURL = 'https://app.zooz.com/mobile/SecuredWebServlet';
		}
		$this->zoozAppID = $appid;
		$this->zoozAppKey = $appkey;
	}

	public function setupCurl()
	{
		// User agent
		curl_setopt($this->curl, CURLOPT_USERAGENT, self::USER_AGENT);

		// Set URL
		curl_setopt($this->curl, CURLOPT_URL, $this->zoozURL);

		//Header fields: ZooZ-Unique-ID, ZooZ-App-Key, ZooZ-Response-Type
		$headers = $this->headers;
		$headers[] = "ZooZ-Unique-ID: {$this->zoozAppID}";
		$headers[] = "ZooZ-App-Key: {$this->zoozAppKey}";
		$headers[] = "ZooZ-Response-Type: NVP";
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

		//If it is a post request
		curl_setopt($this->curl, CURLOPT_POST, 1);

		// Timeout in seconds
		curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);

		// Clean the output
		curl_setopt($this->curl, CURLOPT_HEADER, 0);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

		// Turn off SSL checking
		if ($this->isSandbox == true) 
		{
			curl_setopt ($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
		}
		else
		{
			curl_setopt ($this->curl, CURLOPT_SSL_VERIFYHOST, 1);
			curl_setopt ($this->curl, CURLOPT_SSL_VERIFYPEER, 1);
		}
	}


	public function openTrx($amount, $currency = "USD", $optional = array())
	{
		$this->setupCurl();

		//Mandatory POST fields: cmd, amount, currencyCode
		$post = array();
		$post["cmd"] = "openTrx";
		$post["amount"] = floatval(preg_replace("/[^0-9\.]/", "", $amount));
		$post["currencyCode"] = strtoupper($currency);
		
		//Optional POST fields
		foreach (explode(",", self::OPTIONAL_OPENTRX) as $opt)
		{
			if (isset($optional[$opt]))
			{
				$post[$opt] = $optional[$opt];
			}
		}
		
		curl_setopt ($this->curl, CURLOPT_POSTFIELDS, http_build_query($post));
		$response = curl_exec($this->curl);
		if ($response === false)
		{
			throw new ZoozException("Curl connection to Zooz failed: " . curl_error($this->curl));
		}
		parse_str($response, $result);

		if (!isset($result['statusCode']))
		{
			throw new ZoozException("Zooz failed to return a status code: " . print_r($result, true));
		}
		elseif ($result['statusCode'] !== "0")
		{
			$errcode = $result['statusCode'];
			$err = isset($result['errorMessage']) ? $result['errorMessage'] : "Unknown";
			throw new ZoozException("Zooz return a negative result: $errcode - $err");
		}
		elseif (!isset($result['token']))
		{
			throw new ZoozException("Zooz failed to return a valid token");
		}

		return $result;
	}


	public function verifyTrx($trxId)
	{
		$this->setupCurl();

		//Mandatory POST fields: cmd, amount, currencyCode
		$post = array();
		$post["cmd"] = "verifyTrx";
		$post["trxId"] = trim($trxId);
		
		curl_setopt ($this->curl, CURLOPT_POSTFIELDS, http_build_query($post));
		$response = curl_exec($this->curl);
		if ($response === false)
		{
			throw new ZoozException("Curl connection to Zooz failed: " . curl_error($this->curl));
		}
		parse_str($response, $result);

		if (!isset($result['statusCode']))
		{
			throw new ZoozException("Zooz failed to return a status code: " . print_r($result, true));
		}
		elseif ($result['statusCode'] !== "0")
		{
			$errcode = $result['statusCode'];
			$err = isset($result['errorMessage']) ? $result['errorMessage'] : "Unknown";
			throw new ZoozException("Zooz return a negative result: $errcode - $err");
		}

		return true;
	}

}

