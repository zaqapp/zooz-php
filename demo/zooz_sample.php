<?php

// Flag to indicate whether sandbox environment should be used
$isSandbox = true;

$url;

if ($isSandbox == true) {
	$url = 'https://sandbox.zooz.co/mobile/SecuredWebServlet';
} else {
	$url = 'https://app.zooz.com/mobile/SecuredWebServlet';
}

// is cURL installed yet?
if (!function_exists('curl_init')){
	die('Sorry cURL is not installed!');
}

// OK cool - then let's create a new cURL resource handle
$ch = curl_init();

// Now set some options

// User agent
curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

// Set URL
curl_setopt($ch, CURLOPT_URL, $url);

//Header fields: ZooZ-Unique-ID, ZooZ-App-Key, ZooZ-Response-Type
curl_setopt($ch, CURLOPT_HTTPHEADER, array('ZooZ-Unique-ID: com.zooz.mobileweb.sample','ZooZ-App-Key: my-app-key','ZooZ-Response-Type: NVP'));

//If it is a post request
curl_setopt($ch, CURLOPT_POST, 1);

// Timeout in seconds
curl_setopt($ch, CURLOPT_TIMEOUT, 10);

if (isset($_REQUEST["cmd"]) && $_REQUEST["cmd"] == "openTrx") {
	
	//Mandatory POST fields: cmd, amount, currencyCode
	$postFields = "cmd=openTrx&amount=0.99&currencyCode=USD";
	
	//Optional POST fields:
	$postFields .= "&email=test@zooz.com";
	$postFields .= "&firstName=John";
	$postFields .= "&lastName=Doe";
	
	
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $postFields);

	ob_start();

	curl_exec($ch);

	$result = ob_get_contents();

	ob_end_clean();

	parse_str($result);
		
	if ($statusCode == 0) {

		// Get token from ZooZ server
		$trimmedSessionToken = rtrim($sessionToken, "\n");
			
		// Send token back to page
		echo "var data = {'token' : '" . $trimmedSessionToken . "'}";

	} else if (isset($errorMessage)) {
			
		echo "Error to open transaction to ZooZ server. " . $errorMessage;
			
	}

} else {

	//Post fields: cmd, trxId
	curl_setopt ($ch, CURLOPT_POSTFIELDS, "cmd=verifyTrx&trxId=" . $_REQUEST["trxId"]);

	ob_start();

	curl_exec($ch);

	$result = ob_get_contents();

	ob_end_clean();

	parse_str($result);

	if ($statusCode == 0) {

		//You can use the value of sessionToken to check if it equals the sessionToken you recieved in openTrx
		$trimmedSessionToken = rtrim($sessionToken, "\n");
		
		//trxToken is meant to be used with the ZooZ Extended Server API (See www.zooz.com for more details)
		$trimmedTrxToken = rtrim($trxToken, "\n");
		
		echo "<script>window.location = '/thankyou.html'</script>";
			
	} else {
			
		echo "<script>window.location = '/failed.html'</script>";
			
	}
}


// Close the cURL resource, and free system resources
curl_close($ch);

?>
